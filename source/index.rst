.. Math kurs documentation master file, created by
   sphinx-quickstart on Sat Feb 16 19:59:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Math kurs's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Spis treści:
   :numbered:

   basic/linear_func
   basic/fliniowa_przyklady.ipynb


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
